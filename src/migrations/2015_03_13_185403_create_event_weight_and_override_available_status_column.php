<?php

use Illuminate\Database\Migrations\Migration;

class CreateEventWeightAndOverrideAvailableStatusColumn extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		$table_prefix = Config::get('events-manager::table_prefix');

		Schema::table($table_prefix . 'events', function ($table)
		{
			$table->string('series')->nullable()->after('id');
			$table->integer('weight')->default(0)->after('status');
			$table->boolean('override_available_status')->default(0)->after('weight');
		});
	}
	
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		$table_prefix = Config::get('events-manager::table_prefix');

		Schema::table($table_prefix . 'events', function ($table)
		{
			$table->dropColumn('series');
			$table->dropColumn('weight');
			$table->dropColumn('override_available_status');
		});
	}

}