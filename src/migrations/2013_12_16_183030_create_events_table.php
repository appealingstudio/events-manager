<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEventsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		$table_prefix = Config::get('events-manager::table_prefix');

		Schema::create($table_prefix . 'events', function (Blueprint $table)
		{
			$table->increments('id');

				// Event details
				$table->string('title')->nullable();
				$table->text('description')->nullable();

				// Parent event relationship
				$table->integer('parent_event_id')->default(0);

				// Status
				$table->integer('status')->nullable();

				// Start
				$table->date('start_date')->nullable();
				$table->time('start_time')->nullable();

				// End
				$table->date('end_date')->nullable();
				$table->time('end_time')->nullable();

				// Frequency in which event happens
				$table->enum('recurring_type', array('onetime', 'hourly', 'daily', 'weekly',
					'monthly', 'anually', 'custom'))->default('onetime');

				// For custom frequency we use difference of seconds between occurences
				$table->integer('recurring_custom')->default(0);

				// Reference name of the period in which the event happens
				// used for search. I.e: 'Morning', Monday', 'Febrary'...
				$table->string('recurring_reference_name')->nullable();

				// Limit recurring in time:
				// 
				// start + end => recurring only happens between given dates.
				// NULL start + end => recurring happens until end date.
				// start + NULL end => recurring happens from start date with no end.
				// NULL start + NULL end => recurring happens forever.
				$table->dateTime('recurring_start')->nullable();
				$table->dateTime('recurring_end')->nullable();

				// Item with event
				$table->morphs('eventable');

		    $table->timestamps();
		});
	}
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		$table_prefix = Config::get('events-manager::table_prefix');

		Schema::drop($table_prefix . 'events');
	}

}
