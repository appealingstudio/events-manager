<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddDetailsColumn extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		$table_prefix = Config::get('events-manager::table_prefix');

		Schema::table($table_prefix . 'events', function ($table)
		{
			$table->text('internal_details')->nullable()->after('status');
		});
	}
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		$table_prefix = Config::get('events-manager::table_prefix');

		Schema::table($table_prefix . 'events', function ($table)
		{
			$table->dropColumn('internal_details');
		});
	}
}
