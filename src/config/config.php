<?php 

/**
 * Appealing Studio
 * 
 * Events Manager Package configuration file
 */

return array(

	// Message triggered on exception error
	'table_prefix' => 'appstu_',
);
