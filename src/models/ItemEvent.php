<?php

namespace AppealingStudio\EventsManager;

use LaravelBook\Ardent\Ardent;

use Config;
use Carbon;

class ItemEvent extends Ardent
{
	/**
	 * Class variables
	 */
	protected $table;
	protected $guarded = array('id');

    // --------------------------------------------------------------------

    /**
     * The rules array
     *
     * @var array
     */
    public static $rules = array(
        'start_datetime' => 'required',
        'end_datetime' => 'required',
        'recurring_custom' => 'integer'
    );

	// --------------------------------------------------------------------
	
	/**
	 * Extends Eloquent Model and set table name based on config prefix
	 * 
	 * @param array $attributes
	 */
	public function __construct(array $attributes = array())
	{
        parent::__construct($attributes); // Construct Eloquent Model

        $table_prefix = Config::get('events-manager::table_prefix');
        $this->table = $table_prefix . 'events';
    }

    // --------------------------------------------------------------------

    /**
     * Eventable
     * Polymorphic method to allow multiple relationship
     * 
     * @return Related Instance
     */
    public function eventable()
    {
        return $this->morphTo();
    }

    // --------------------------------------------------------------------

    /**
     * Child events
     * 
     * @return ItemEvent
     */
    public function childs()
    {
        return $this->hasMany('AppealingStudio\EventsManager\ItemEvent', 'parent_event_id');
    }

    // --------------------------------------------------------------------

    /**
     * Parent event
     * 
     * @return ItemEvent
     */
    public function parent()
    {
        return $this->belongsTo('AppealingStudio\EventsManager\ItemEvent', 'parent_event_id');
    }

	// --------------------------------------------------------------------

    /**
     * Define carbon type for date fields
     * 
     * @return array
     */
	public function getDates()
	{
	    return array('created_at', 'updatet_at', 'deleted_at');
	}

    // --------------------------------------------------------------------

    /**
     * Get recurring reference name based on recurring type and start date
     * 
     * @return mixed
     */
    public function getRecurringReferenceNameAttribute()
    {
        if (isset($this->attributes['recurring_reference_name']) && ! empty($this->attributes['recurring_reference_name']))
        {
            return $this->attributes['recurring_reference_name'];
        }
        else
        {
            return $this->start_datetime->format('l');
        }
    }

    // --------------------------------------------------------------------

    /**
     * Get event start date & time
     * 
     * @return Carbon
     */
    public function getStartDatetimeAttribute($value)
    {
        return Carbon::parse($value);
    }

    // --------------------------------------------------------------------

    /**
     * Get event end date & time
     * 
     * @return Carbon
     */
    public function getEndDatetimeAttribute($value)
    {
        return Carbon::parse($value);
    }

    // --------------------------------------------------------------------

    /**
     * Get event start time
     * 
     * @return Carbon
     */
    public function getStartTimeAttribute($value)
    {
        return Carbon::parse($value);
    }

    // --------------------------------------------------------------------

    /**
     * Get event end time
     * 
     * @return Carbon
     */
    public function getEndTimeAttribute($value)
    {
        return Carbon::parse($value);
    }

    // --------------------------------------------------------------------

    /**
     * Get next recurring ocurrence
     * 
     * TODO:
     * - Perform search for remaining recurring types
     * - Add timezone support
     * 
     * @param  ItemEvent $event
     * @return ItemEvent
     */
    public function getNextOcurrence($datetimeReference = NULL)
    {
        $datetimeReference = ($datetimeReference == NULL) ? Carbon::now() : Carbon::parse($datetimeReference);

        switch ($this->recurring_type)
        {
            case 'weekly':
                $datetimeReference->startOfDay();
                $datetimeReference->subSecond();

                $nextDate = Carbon::createFromTimeStamp(
                    strtotime('next ' . $this->recurring_reference_name, $datetimeReference->timestamp)
                );

                $this->start_date = $nextDate->toDateString();
                $this->end_date = $nextDate->toDateString();
                break;
        }
    }

    // --------------------------------------------------------------------

    /**
     * Get all occurrences in a given datetime range
     *
     * TODO:
     * - Check recurring limits
     * 
     * @param  integer $month
     * @param  integer $year
     * @return array
     */
    public function getDateRangeAvailability($rangeStart, $rangeEnd)
    {
        $result = NULL;

        // Event not available
        if ($this->status == FALSE)
        {
            return $result;
        }

        $rangeStart = $datetimeReference = Carbon::parse($rangeStart);
        $rangeEnd = Carbon::parse($rangeEnd);

        // Iterate until range ends
        $intoRange = FALSE;
        do {
            $this->getNextOcurrence($datetimeReference);

            // If override event type, check if we are still in its date range limit
            $overrideEventIntoRange = TRUE;
            if ($this->weight > 0)
            {
                $overrideEventIntoRange = $this->intoRange(
                    $this->recurring_start,
                    $this->recurring_end,
                    $this->startDateTime->toDateTimeString(),
                    $this->endDateTime->toDateTimeString()
                );
            }

            $intoRange = $this->intoRange(
                $rangeStart->toDateTimeString(),
                $rangeEnd->toDateTimeString(),
                $this->startDateTime,
                $this->startDateTime
            );

            if ($intoRange)
            {
                if ($overrideEventIntoRange)
                {
                    $result[] = clone($this);
                }

                // Adjust next date time reference to not enter on an infinite loop
                switch ($this->recurring_type)
                {
                    case 'weekly':
                        $datetimeReference = $this->startDateTime->addDay();
                        break;
                }

                // Onetime event found, let's exit
                if ($this->recurring_type == 'onetime')
                {
                    $intoRange = FALSE;
                }
            }
        } while ($intoRange);

        return $result;
    }

    // --------------------------------------------------------------------

    /**
     * Checks if event starts into a given datetime range
     *
     * TODO:
     * - Add support to check that full event fits into date range, not only event start
     * 
     * @param  string $rangeStart
     * @param  string $rangeEnd
     * @return boolean
     */
    public function intoRange($rangeStart, $rangeEnd, $eventStart, $eventEnd)
    {
        $rangeStartTimestamp = Carbon::parse($rangeStart)->timestamp;
        $rangeEndTimestamp = Carbon::parse($rangeEnd)->timestamp;

        $eventStartTimestamp = Carbon::parse($eventStart)->timestamp;
        $eventEndTimestamp = Carbon::parse($eventEnd)->timestamp;

        return ($rangeStartTimestamp <= $eventStartTimestamp && $eventEndTimestamp <= $rangeEndTimestamp);
    }

    // --------------------------------------------------------------------

    /**
     * Start date setter
     */
    public function setStartDateAttribute($value)
    {
        $this->attributes['start_date'] = ($value == NULL) ? NULL : Carbon::parse($value)->toDateString();
    }

    // --------------------------------------------------------------------

    /**
     * End date setter
     */
    public function setEndDateAttribute($value)
    {
        $this->attributes['end_date'] = ($value == NULL) ? NULL : Carbon::parse($value)->toDateString();
    }

    // --------------------------------------------------------------------

    /**
     * Start time setter
     */
    public function setStartTimeAttribute($value)
    {
        $this->attributes['start_time'] = Carbon::parse($value)->toTimeString();
    }

    // --------------------------------------------------------------------

    /**
     * End date setter
     */
    public function setEndTimeAttribute($value)
    {
        $this->attributes['end_time'] = Carbon::parse($value)->toTimeString();
    }

    // --------------------------------------------------------------------

    /**
     * Recurring Start setter
     */
    public function setRecurringStartAttribute($value)
    {
        $this->attributes['recurring_start'] = ($value == NULL) ? NULL : Carbon::parse($value)->toDateTimeString();
    }

    // --------------------------------------------------------------------

    /**
     * Recurring End setter
     */
    public function setRecurringEndAttribute($value)
    {
        $this->attributes['recurring_end'] = Carbon::parse($value)->toDateTimeString();
    }

    // --------------------------------------------------------------------

    /*public function getEventStartAttribute()
    {
        return $this->_getEventDateTimeAttribute('startDateTime');
    }

    // --------------------------------------------------------------------

    public function getEventEndAttribute()
    {
        return $this->_getEventDateTimeAttribute('endDateTime');
    }

    // --------------------------------------------------------------------

    private function _getEventDateTimeAttribute($property)
    {
        switch ($this->recurring_type)
        {
            case 'weekly':
                if (is_null($this->$property))
                {
                    $eventDate = Carbon::parse('last ' . $this->recurring_reference_name)->subWeeks(5);
                    $eventTime = ($property == 'startDateTime')
                        ? $this->start_time->toTimeString()
                        : $this->end_time->toTimeString();

                    return Carbon::parse($eventDate->toDateString() . ' ' . $eventTime);
                }
                break;
        }

        return $this->$property;
    }*/

}
