<?php

namespace AppealingStudio\EventsManager\Traits;

use Carbon;

use Sabre\VObject\Component\VEvent;
use Sabre\VObject\Component\VCalendar;
use Sabre\VObject\Recur\EventIterator;
use Sabre\VObject\Property\ICalendar\DateTime;

use AppealingStudio\EventsManager\ItemEvent;
use Illuminate\Database\Eloquent\Collection;

/**
 * Eventable trait.
 *
 * Extend models that will have events by simply adding this trait.
 */
trait HasEvents
{
    /**
     * Public properties
     */
    public $eventSelected = NULL;

    // --------------------------------------------------------------------

    /**
     * Protected properties
     */
    protected $_vCalendar;
    protected $_eventsMaxWeight;
    protected $_weightyClosedEvents;

    // --------------------------------------------------------------------

	/**
	 * Events relationship
	 * 
	 * @return Events
	 */
    public function itemEvents()
    {
        return $this->morphMany('AppealingStudio\EventsManager\ItemEvent', 'eventable');
    }

    // --------------------------------------------------------------------

    /**
     * Checks if parent event is onetime
     * 
     * @return boolean
     */
    public function getOneTimeParentEventAttribute()
    {
        $result = NULL;

        foreach ($this->itemEvents as $itemEvent)
        {
            // If the item is one time, there should be only one event
            // which must be parent and 'onetime' recurring type and must have weight 0
            if ($itemEvent->recurring_type == 'onetime' && $itemEvent->parent_event_id == 0 && $itemEvent->weight == 0)
            {
                $result = $itemEvent;
            }
        }

        return $result;
    }

    // --------------------------------------------------------------------

    /**
     * Check if current parent model has a onetime event
     * 
     * @return boolean
     */
    public function getIsOneTimeEventAttribute()
    {
        $event = $this->oneTimeParentEvent;

        if ($event == NULL)
        {
            return NULL;
        }

        return ($event != FALSE && $event->status == 1);
    }

    // --------------------------------------------------------------------

    /**
     * Get parent events
     * 
     * @return Collection
     */
    public function getParentEventsAttribute()
    {
        return $this->itemEvents->filter(function($itemEvent)
        {
            return ($itemEvent->parent_event_id == 0);
        });
    }

    // --------------------------------------------------------------------

    /**
     * Get weekly schedule
     * 
     * @return Collection
     */
    public function getWeeklyScheduleAttribute()
    {
        // Get weekly parent events
        $schedule = $this->itemEvents->filter(function($itemEvent)
        {
            return ($itemEvent->recurring_type == 'weekly'
                && $itemEvent->parent_event_id == 0
                && $itemEvent->weight == 0
                && $itemEvent->status == 1);
        });

        // Sort them by day & time
        $schedule->sortBy(function($itemEvent)
        {
            return Carbon::parse(
                'next ' . $itemEvent->start_datetime->format('l') . ' ' . $itemEvent->start_datetime->toTimeString()
            )->timestamp;
        });

        // And integrate into right day, setting the correct end time to the unique final event for every week day
        $currentDay = NULL;
        foreach ($schedule as $key => $event)
        {
            if (is_null($currentDay) OR $event->start_datetime->format('l') != $currentDay)
            {
                $currentDay = $event->start_datetime->format('l');
                $daySchedule = $event;
            }
            else
            {
                $daySchedule->end_datetime = $event->end_datetime;
                unset($schedule[$key]);
            }
        }

        return $schedule;
    }

    // --------------------------------------------------------------------

    /**
     * Gets the selected event date & time
     *
     * @return Carbon
     */
    /*public function getSelectedEventDateRangeAttribute()
    {
        return array(
            'start' => $this->eventSelected->start_datetime,
            'end'   => $this->eventSelected->end_datetime
        );
    }

    // --------------------------------------------------------------------

    /**
     * Get all events owned by an instance of the related model
     * 
     * @return void
     */
    /*public function getAllEventsInDateRangeAttribute($start, $end)
    {
        return clone $this->itemEvents;
    }

    // --------------------------------------------------------------------

    /**
     * Select event to 'play' with for related model instance
     * 
     * @param  mixed $event
     * @return ItemEvent
     */
    public function selectEvent($event)
    {
        // We expect an ItemEvent instance
        $result = $event;

        // But also can be an event_id
        if (is_numeric($event))
        {
            if ($this->itemEvents->contains($event))
            {
                $result = $this->itemEvents->find($event);

                if ($result->recurring_type != 'onetime')
                {
                    $result = $this->closestEventTo();   
                }
            }
            else
            {
                $result = FALSE;
            }
        }

        $this->eventSelected = $result;

        return $result;
    }

    // --------------------------------------------------------------------

    /**
     * Generates a static child event(onetime) of passed event and set as selected
     * It does the same for already onetime events
     * 
     * @param  ItemEvent $event
     * @return void
     */
    public function setStaticEvent($event)
    {
        if ($event->parent_event_id == 0)
        {
            if ($event->recurring_type != 'onetime')
            {
                $event->recurring_type = 'onetime';
                //$event->recurring_reference_name = NULL;
                $event->parent_event_id = $event->id;
            }

            $itemEvent = ItemEvent::create($event->toArray());
        }

        $this->eventSelected = (isset($itemEvent)) ? $itemEvent : $event;
    }

    // --------------------------------------------------------------------

    /**
     * Get closest event to reference date passed
     *
     * TODO - Add a paramater to check only passed event list
     * 
     * @param string  $referenceDate
     * @param boolean $onlyFuture
     * @return ItemEvent
     */
    public function closestEventTo($referenceDate = NULL, $onlyFuture = TRUE)
    {
        $events = NULL;
        $referenceDate = is_null($referenceDate) ? Carbon::now() : Carbon::parse($referenceDate);

        $this->createVCalendar();

        $eventSelected = NULL;
        $lastDifference = NULL;
        foreach ($this->_vCalendar->select('VEVENT') as $vEvent)
        {
            $eventDetails = array('id' => $vEvent->UID->getValue());

            if (is_null($vEvent->RRULE))
            {
                // For one time events, let's get its start / end date times
                $eventDetails['start_datetime'] = Carbon::parse($vEvent->DTSTART->getValue());
                $eventDetails['end_datetime'] = Carbon::parse($vEvent->DTEND->getValue());
            }
            else
            {
                // For recurring, let's calculate the closest to the provided date
                $iterator = new EventIterator($vEvent);
                $iterator->fastForward($referenceDate);

                $eventDetails['start_datetime'] = Carbon::instance($iterator->getDTStart());
                $eventDetails['end_datetime'] = Carbon::instance($iterator->getDTEnd());
            }

            $difference = $referenceDate->diffInSeconds($eventDetails['start_datetime'], false);

            if ($difference > 0)
            {
                if (is_null($lastDifference) OR $lastDifference > $difference)
                {
                    $lastDifference = $difference;
                    $eventSelected = $eventDetails;
                }
            }
        }

        if (is_null($eventSelected))
        {
            return NULL;
        }

        $event = $this->itemEvents->find($eventSelected['id']);

        $event->start_datetime = $eventSelected['start_datetime'];
        $event->end_datetime = $eventSelected['end_datetime'];

        return $event;
    }

    // --------------------------------------------------------------------

    /**
     * Check if two events have the same date & time
     * 
     * @param  ItemEvent  $event
     * @param  ItemEvent  $parentEvent
     * @return boolean
     */
    /*public function eventsHaveSameDateTime($event, $parentEvent)
    {
        return ($event->start_datetime == $parentEvent->start_datetime && $event->end_datetime == $parentEvent->end_datetime);
    }

    // --------------------------------------------------------------------

    /**
     * Get month recurring availability
     * 
     * @param  integer $month
     * @param  integer $year
     * @return array
     */
    public function getMonthlyAvailability($month, $year, $excludePastEvents = TRUE, $removeOverrideEvents = FALSE)
    {
        $rangeReferenceDate = Carbon::createFromDate($year, $month);

        $availabilityResult = $this->getEventsDateRangeAvailability(
            $rangeReferenceDate->startOfMonth()->toDateTimeString(),
            $rangeReferenceDate->endOfMonth()->toDateTimeString(),
            $excludePastEvents,
            $removeOverrideEvents
        );

        $monthlyResult = array();
        foreach ($availabilityResult as $event)
        {
            $monthlyResult[$event->start_datetime->day][] = $event;
        }

        ksort($monthlyResult);

        return $monthlyResult;
    }

    // --------------------------------------------------------------------

    /**
     * Get date range availability of all events owned by this parent instance
     * 
     * @param  string  $rangeStart
     * @param  string  $rangeEnd
     * @param  boolean $excludePastEvents
     * @return array
     */
    public function getEventsDateRangeAvailability($rangeStart, $rangeEnd, $excludePastEvents = TRUE,
        $removeOverrideEvents = FALSE)
    {
        $this->createExpandedVCalendar($rangeStart, $rangeEnd, $excludePastEvents);

        // Build events availability collection
        $availabilityResult = new Collection;
        foreach ($this->_vCalendar->select('VEVENT') as $vEvent)
        {
            // Don't include event if is an override status event and remove those events required
            if (isset($vEvent->overrideStatus) && $removeOverrideEvents == TRUE)
            {
                continue;
            }

            // Find original event
            $dbEvent = clone($this->itemEvents->find($vEvent->UID->getValue()));

            // Adjust start/end datetimes to current ocurrence
            $dbEvent->start_datetime = Carbon::parse($vEvent->DTSTART->getValue());
            $dbEvent->end_datetime = Carbon::parse($vEvent->DTEND->getValue());

            // Insert event into results
            $availabilityResult->add($dbEvent);
        }

        return $availabilityResult;
    }

    // --------------------------------------------------------------------

    /**
     * Gets Item vCalendar events into required date range
     * 
     * @param  string  $rangeStart
     * @param  string  $rangeEnd
     * @param  boolean $excludePastEvents
     * @param  string  $viewType
     * @return void
     */
    public function createExpandedVCalendar($rangeStart, $rangeEnd, $excludePastEvents = TRUE, $viewType = 'month')
    {
        $this->createVCalendar();
        //echo '<pre>'; var_dump($this->_vCalendar->serialize()); echo '</pre>'; die;

        $rangeStart = Carbon::parse($rangeStart);

        // If we must exclude past event, adjust range start depending on view type if current date is included
        if ($excludePastEvents)
        {
            switch ($viewType)
            {
                // Default view is month
                default:
                    if ($rangeStart->startOfMonth() == Carbon::now()->startOfMonth())
                    {
                        $rangeStart = Carbon::now();
                    }
                    break;
            }            
        }

        // Expand calendar to requested date range
        $this->_vCalendar->expand($rangeStart, Carbon::parse($rangeEnd));

        //echo '<pre>'; var_dump($this->_vCalendar->serialize()); echo '</pre>'; die;
    }

    // --------------------------------------------------------------------

    /**
     * Creates an vCalendar version with all the item events
     * 
     * @return Sabre\VObject\Component\VCalendar
     */
    public function createVCalendar()
    {
        $itemEvents = $this->itemEvents;
        $itemEvents->load('childs');

        // Create vCalendar using item events
        $maxWeight = 0;
        $this->_vCalendar = new VCalendar();
        foreach ($itemEvents as $itemEvent)
        {
            if ($itemEvent->parent_event_id == 0 && $itemEvent->status == 1)
            {
                $vEvent = $this->_createVEvent($itemEvent);
                if ($maxWeight < $vEvent->weight->getValue())
                {
                    $maxWeight = $vEvent->weight->getValue();
                }
            }
        }

        // If weighty events exist, check possible overrides
        if ($maxWeight > 0)
        {        
            do
            {
                foreach ($this->_vCalendar->select('VEVENT') as $vWeightyEvent)
                {
                    if ($vWeightyEvent->weight->getValue() == $maxWeight)
                    {
                        $this->_checkWeightyEventOverrides($vWeightyEvent, $maxWeight);
                    }
                }

                $maxWeight--;
            }
            while ($maxWeight > 0);
        }

        //echo '<pre>'; var_dump($this->_vCalendar->serialize()); echo '</pre>'; die;

        return $this->_vCalendar;        
    }

    // --------------------------------------------------------------------

    /**
     * Checks weighty event overrides
     * 
     * @param  Sabre\VObject\Component\VEvent $vWeightyEvent
     * @param  integer $maxWeight
     * @return void
     */
    private function _checkWeightyEventOverrides($vWeightyEvent, $maxWeight)
    {
        $vWeightyEventOccurrences = $this->_getAllvEventOcurrences($vWeightyEvent);

        foreach ($vWeightyEventOccurrences as $vWeightyEventOccurrence)
        {
            // Get day limits for current occurrence
            $vWeightyEventOccurrenceDayStart = Carbon::parse($vWeightyEventOccurrence->DTSTART->getValue())->startOfDay();
            $vWeightyEventOccurrenceDayEnd = Carbon::parse($vWeightyEventOccurrence->DTEND->getValue())->endOfDay();

            foreach ($this->_vCalendar->select('VEVENT') as $key => $vEvent)
            {
                $eventWeight = $vEvent->weight->getValue();
                if ($eventWeight < $maxWeight)
                {
                    // Check if lowest event is in weighty event occurrence day
                    if ($vEvent->isInTimeRange($vWeightyEventOccurrenceDayStart, $vWeightyEventOccurrenceDayEnd))
                    {
                        if (is_null($vEvent->RRULE))    // If one time event, remove from calendar
                        {
                            $this->_vCalendar->remove($vEvent);
                        }
                        else    // Otherwise, create an exception date
                        {
                            $vEventIterator = new EventIterator($vEvent);
                            $vEventIterator->fastForward($vWeightyEventOccurrenceDayStart);

                            if (is_null($this->_vCalendar->children[$key]->EXDATE))
                            {
                                $this->_vCalendar->children[$key]->add('EXDATE', array($vEventIterator->getDTStart()));
                            }
                            else
                            {
                                $exdates = array_merge(
                                    $this->_vCalendar->children[$key]->EXDATE->getDateTimes(),
                                    array($vEventIterator->getDTStart())
                                );

                                $this->_vCalendar->children[$key]->EXDATE->setValue($exdates);
                            }
                        }
                    }
                }
            }                        
        }
    }

    // --------------------------------------------------------------------

    /**
     * Gets all occurrences of the provided vEvent
     * 
     * @param  Sabre\VObject\Component\VEvent $vEvent
     * @return array
     */
    private function _getAllvEventOcurrences($vEvent)
    {
        $occurrences = array();

        if (is_null($vEvent->RRULE))
        {
            $occurrences[] = $vEvent;
        }
        else
        {
            $vEventStart = Carbon::parse($vEvent->DTSTART->getValue());

            $iterator = new EventIterator($vEvent);

            if ($iterator->isInfinite())
            {
                // Cannot generate all occurrences for infinite events
                // 
                // TODO: Create exception
                // 
                return NULL;
            }

            $rRule = $vEvent->RRULE->getParts();
            $iterator->fastForward($vEventStart);

            while($iterator->valid() && $iterator->getDTStart() < Carbon::parse($rRule['UNTIL']))
            {
                if ($iterator->getDTEnd() >= $vEventStart) {
                    $occurrences[] = $iterator->getEventObject();
                }

                $iterator->next();
            }
        }

        return $occurrences;
    }

    // --------------------------------------------------------------------

    /**
     * Creates an vEvent and inserts it into provided vCalendar
     * 
     * @param  ItemEvent $itemEvent
     * @return void
     */
    private function _createVEvent($itemEvent)
    {
        $eventData = array(
            'DTSTART' => ($itemEvent->override_available_status == 1)
                ? $itemEvent->start_datetime->startOfDay()
                : $itemEvent->start_datetime,
            'DTEND' => ($itemEvent->override_available_status == 1)
                ? $itemEvent->end_datetime->endOfDay()
                : $itemEvent->end_datetime
        );

        switch ($itemEvent->recurring_type)
        {
            case 'weekly':
                $eventData['RRULE'] = 'FREQ=WEEKLY';
                break;

            case 'monthly':
                $eventData['RRULE'] = 'FREQ=MONTHLY';
                break;

            case 'annually':
                $eventData['RRULE'] = 'FREQ=YEARLY';
                break;
        }

        // Add recurring end if not onetime event and it exists
        if ($itemEvent->recurring_type != 'onetime' && ! is_null($itemEvent->recurring_end))
        {
            $eventData['RRULE'] .= ';UNTIL=' . Carbon::parse($itemEvent->recurring_end)->format('Ymd\THis\Z');
        }

        if ( ! $itemEvent->childs->isEmpty())
        {
            // For each existing onetime child,
            // include it and exclude the recurring version
            foreach ($itemEvent->childs as $child)
            {
                $this->_createVEvent($child);
                $eventData['EXDATE'][] = $child->start_datetime;
            }
        }
        
        $vEvent = $this->_vCalendar->add('VEVENT', $eventData);

        $vEvent->UID = $itemEvent->id;
        $vEvent->weight = $itemEvent->weight;
        $vEvent->parent_event_id = $itemEvent->parent_event_id;

        if ($itemEvent->override_available_status == 1)
        {
            $vEvent->overrideStatus = TRUE;
        }

        return $vEvent;
    }

    // --------------------------------------------------------------------

    /**
     * Store singel event
     * 
     * @param  array  $eventInput
     * @param  boolean $save
     * @return ItemEvent
     */
    public function storeSingleEvent($eventInput, $save = TRUE)
    {
        $event = NULL;
        $id = array_get($eventInput, 'id');

        if (is_numeric($id) && ! empty($id))
        {
            $event = ItemEvent::find(array_get($eventInput, 'id'));
        }

        if (is_null($event))
        {
            $event = new ItemEvent;                
        }

        /*$event = (is_numeric($id) && ! empty($id))
            ? ItemEvent::find(array_get($eventInput, 'id'))
            : new ItemEvent; */

        // Remove recurring custom, not needed for database record
        if (isset($eventInput['recurring_custom']))
        {
            unset($eventInput['recurring_custom']);
        }

        $event->fill($eventInput);

        if ($save)
        {
            $event->save();
            $this->itemEvents()->save($event);
        }
        else
        {
            $event->validate();
            $this->itemEvents->add($event);
        }

        return $event;
    }

    // --------------------------------------------------------------------

    /**
     * Store multiple events
     * 
     * @param  array  $eventsInput
     * @param  boolean $save
     * @param  boolean $closeExistingEvents
     * @return ItemEvent array
     */
    public function storeMultipleEvents($eventsInput, $save = TRUE, $closeExistingEvents = TRUE, $leaveWeighty = TRUE)
    {
        $result = array();
        foreach ($eventsInput as $eventInput)
        {
            if (isset($eventInput['recurring_custom']) && $eventInput['recurring_custom'] > 0)
            {
                switch ($eventInput['recurring_type'])
                {
                    case 'weekly':
                        $result = array_merge($result, $this->storeWeeklyHourlyEvents($eventInput, $save));
                        break;
                }
            }
            else
            {
                $result[] = $this->storeSingleEvent($eventInput, $save);        
            }
        }

        if ($save && $closeExistingEvents)
        {
            $this->closeExistingEvents($result, $leaveWeighty);
        }

        return $result;
    }

    // --------------------------------------------------------------------

    /**
     * Store hourly events for weekly days
     * 
     * @param  array $eventInput
     * @param  boolean $save
     * @return array
     */
    public function storeWeeklyHourlyEvents($eventInput, $save)
    {
        $result = array();
        unset($eventInput['id']);

        $currentTime = Carbon::parse($eventInput['start_datetime']);
        $endTime = Carbon::parse($eventInput['end_datetime']);

        while ($currentTime->lt($endTime))
        {
            $eventInput['start_datetime'] = $currentTime->toDateTimeString();
            $currentTime->addMinutes($eventInput['recurring_custom']);
            $eventInput['end_datetime'] = $currentTime->toDateTimeString();

            $result[] = $this->storeSingleEvent($eventInput, $save);
        }

        return $result;
    }

    // --------------------------------------------------------------------

    /**
     * Close existing events
     * 
     * @return void
     */
    public function closeExistingEvents($result, $leaveWeighty = TRUE)
    {
        foreach ($this->itemEvents as $itemEvent)
        {
            if ($itemEvent->parent_event_id == 0)
            {           
                $opened = FALSE;
                foreach ($result as $event)
                {
                    if ($event['id'] == $itemEvent->id)
                    {
                        $opened = TRUE;
                        break;
                    }
                }

                if ( ! $opened && ($leaveWeighty == FALSE OR ($leaveWeighty == TRUE && $itemEvent->weight == 0)))
                {
                    $this->_deleteOrDisableEvent($itemEvent);
                }
            }
        }
    }

    // --------------------------------------------------------------------

    /**
     * Gets max weight of events inside date range provided
     * 
     * @param  string $rangeStart
     * @param  string $rangeEnd
     * @return integer
     */
    public function getEventsDateRangeMaxWeight($rangeStart, $rangeEnd)
    {
        $availabilityResult = $this->getEventsDateRangeAvailability($rangeStart, $rangeEnd, FALSE);

        $maxWeight = 0;
        foreach ($availabilityResult as $event)
        {
            if ($event->weight > $maxWeight)
            {
                $maxWeight = $event->weight;
            }
        }

        return $maxWeight;
    }
    
    // --------------------------------------------------------------------

    /**
     * Deletes all events from a series
     * 
     * @param  string $series
     * @return void
     */
    public function deleteSeries($series)
    {
        $events = $this->itemEvents->filter(function($event) use($series)
        {
            return $event->series == $series;
        });

        $this->_deleteEventsCollection($events);

        return TRUE;
    }

    // --------------------------------------------------------------------

    /**
     * Deletes required event
     * 
     * @param  string $id
     * @return void
     */
    public function deleteEvent($id)
    {
        $event = $this->getEventById($id);

        if ( ! is_null($event))
        {        
            $this->_deleteOrDisableEvent($event);
        }

        return TRUE;
    }

    // --------------------------------------------------------------------

    /**
     * Deletes required event and all the followings
     * 
     * @param  string $id
     * @return void
     */
    public function deleteFollowingEvents($id)
    {
        $event = $this->getEventById($id);

        if ( ! is_null($event))
        {
            $eventStart = $event->start_datetime;
            $followingEvents = $this->itemEvents->filter(function($event) use($eventStart)
            {
                if ( ! is_null($event->start_datetime))
                {
                    return $event->start_datetime->gte($eventStart);
                }

                return FALSE;
            });

            $this->_deleteEventsCollection($followingEvents);
        }

        return TRUE;
    }

    // --------------------------------------------------------------------

    /**
     * Gets tasting concrete event by its id
     * 
     * @param  integer $id
     * @return ItemEvent
     */
    public function getEventById($id)
    {
        $events = $this->itemEvents->filter(function($event) use($id)
        {
            return $event->id == $id;
        });

        if ( ! $events->isEmpty())
        {        
            return $events->first();
        }

        return NULL;
    }

    // --------------------------------------------------------------------

    /**
     * Iterates over passed event collection and deletes or disables them
     * 
     * @param  Collection $events
     * @return void
     */
    private function _deleteEventsCollection($events)
    {
        if ( ! $events->isEmpty())
        {        
            foreach ($events as $event)
            {
                if ($event->parent_event_id == 0)
                {
                    $this->_deleteOrDisableEvent($event);
                }
            }
        }
    }

    // --------------------------------------------------------------------

    /**
     * Deletes or disables (if it has childs) and event
     * 
     * @param  ItemEvent $event
     * @return void
     */
    private function _deleteOrDisableEvent($event)
    {
        if ($event->childs()->count() == 0 )
        {
            // if event doesn't have childs, we can safely delete it
            $event->delete();
        }
        else
        {
            // else, we set parent and children as unavailable
            $event->status = FALSE;
            $event->save();

            foreach ($event->childs as $child)
            {
                $child->status = FALSE;
                $child->save();
            }
        }
    }

    // --------------------------------------------------------------------

    /**
     * Remove parent events with statistics version from an events array
     * 
     * @param  array $events
     * @return array
     */
    /*protected function _removeParentEventsWithStaticVersion($events)
    {
        if (count($events) > 1)
        {
            foreach ($events as $key => $event)
            {
                if (isset($event->parent_event_id) &&  $event->parent_event_id == 0)
                {                
                    foreach ($events as $keyToCompare => $eventToCompare)
                    {
                        if ($eventToCompare->parent_event_id == $event->id &&
                            $this->eventsHaveSameDateTime($event, $eventToCompare))
                        {
                            unset($events[$key]);
                        }
                    }
                }
            }
        }

        return $events;
    }*/
}
