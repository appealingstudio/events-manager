<?php

namespace AppealingStudio\EventsManager;

use AppealingStudio\EventsManager\ItemEvent;

class ItemEventsRepository
{
	/**
	 * Finds and event by attributes or create it,
	 * then updates it with given data
	 * 
	 * @param  array $match
	 * @param  array $data
	 * @return ItemEvent
	 */
	public function firstOrCreateItemEventByAttributes($match, $data)
	{
		$event = ItemEvent::firstOrCreate($match);
		$event->update($data);

		return $event;
	}

	// --------------------------------------------------------------------

	/**
	 * Delete events by attributes
	 * 
	 * @param  array $attributes
	 * @return void
	 */
	public function deleteByAttributes($attributes)
	{
		do
		{
			$event = ItemEvent::firstByAttributes($attributes);

			if ($event != NULL)
			{
				$event->delete();
			}
		} while ($event != NULL);
	}

	// --------------------------------------------------------------------

	/**
	 * Attach multiple events into related model
	 * 
	 * @param  EloquentModel $relatedModel
	 * @param  array $events
	 * @return void
	 */
	public function updateOrCreateEvents($relatedModel, $events)
	{
		foreach ($events as $event)
		{
			$itemEvent = $relatedModel->itemEvents()->find($event->id);

			if ($itemEvent == NULL)
			{
				$itemEvent = new ItemEvent;
				$itemEvent->fill($event->toArray());
				$relatedModel->itemEvents()->save($itemEvent);
			}
			else
			{
				$itemEvent->update($event->toArray());
			}
		}
	}
}
