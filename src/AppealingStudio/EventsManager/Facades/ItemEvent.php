<?php namespace AppealingStudio\EventsManager\Facades;

use Illuminate\Support\Facades\Facade;

class ItemEvent extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'itemEvent'; }

}